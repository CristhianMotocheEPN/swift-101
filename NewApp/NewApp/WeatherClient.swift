//
//  WeatherClient.swift
//  NewApp
//
//  Created by Cristhian Motoche on 5/5/17.
//  Copyright © 2017 Cristhian Motoche. All rights reserved.
//

import Foundation

class WeatherClient {
    func urlByCity(city:String, completionHandler: @escaping (String) -> ()) -> Void {
        let appid = "0f2490329e79b7573fea9a52655df241"
        let urlString = "http://samples.openweathermap.org/data/2.5/weather?q=\(city)&appid=\(appid)"
        let url = URL(string: urlString)!
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: {
            (data, response, error) in
            
            // Logging
            print("Sending request for city: \(city)")
            
            if let errorMessage = error {
                print("[ERROR]: \(errorMessage)")
                return
            } else {
                guard let dataResponse = data else {
                    print("There is no data")
                    return
                }
                
                do {
                    let jsonData = try JSONSerialization.jsonObject(with: dataResponse, options: [])
                    let jsonDict = jsonData as! NSDictionary
                    let weatherArray = jsonDict["weather"] as! NSArray
                    let weatherDictionary = weatherArray[0] as! NSDictionary
                    completionHandler((weatherDictionary["main"] as? String)!)
                }
                catch {
                    print("Seliarization error")
                    return
                }
            }
        })
        task.resume()
    }
}
