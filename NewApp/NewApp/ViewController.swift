//
//  ViewController.swift
//  NewApp
//
//  Created by Cristhian Motoche on 28/4/17.
//  Copyright © 2017 Cristhian Motoche. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var userTextField:
        UITextField!
    
    @IBOutlet weak var passwordTextField:
        UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func loginButtonPressed(sender: Any) {

        let usuario:String = self.userTextField.text ?? "Not input"
        guard let password = self.passwordTextField.text else {
            return
        }
        
        switch (usuario, password) {
        case ("Cristhian", "Motoche"):
            performSegue(withIdentifier: "toWelcomeViewControllerSegue",
                         sender: self)
        default:
            print("Not allowed to in!")
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // self.userTextField.resignFirstResponder()
        // self.passwordTextField.resignFirstResponder()
        view.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toWelcomeViewControllerSegue" {
            let destination = segue.destination as! WelcomeViewController
            destination.message = self.userTextField.text
        }
    }
}
