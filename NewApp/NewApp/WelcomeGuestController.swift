//
//  WelcomeGuestController.swift
//  NewApp
//
//  Created by Cristhian Motoche on 5/5/17.
//  Copyright © 2017 Cristhian Motoche. All rights reserved.
//

import UIKit
import CoreLocation

class WelcomeGuestController: UIViewController,
    CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    
    @IBOutlet weak var weatherGuestLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
    }

    override func viewWillAppear(_ animated: Bool) {
        let weatherClient = WeatherClient()
        weatherClient.urlByCity(city: "Quito") { str in
            DispatchQueue.main.async {
                self.weatherGuestLabel!.text = str
            }
        }
    }
}
