//
//  WelcomeViewController.swift
//  NewApp
//
//  Created by Cristhian Motoche on 28/4/17.
//  Copyright © 2017 Cristhian Motoche. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {

    var message:String?
    
    @IBOutlet weak var welcomeMessageLabel:
        UILabel!

    @IBOutlet weak var weatherMessageLabel:
        UILabel!

    @IBOutlet weak var weatherSearchBar: UISearchBar!

    @IBAction func searchButton(_ sender: UIButton) {
        let weatherClient = WeatherClient()
        weatherClient.urlByCity(city: self.weatherSearchBar.text ?? "Quito") { str in
            DispatchQueue.main.async {
                self.weatherMessageLabel!.text = str
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.welcomeMessageLabel.text = message!
    }
}
